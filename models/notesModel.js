const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotesSchema = new Schema({
  userId: {
    required: true,
    type: String,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    required: true,
    type: String,
  },
  createdDate: {
    type: String,
    default: new Date(),
  },
});

module.exports = mongoose.model('note', NotesSchema);
