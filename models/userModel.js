const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    required: true,
    unique: true,
    type: String,
  },
  password: {
    required: true,
    type: String,
  },
  createdDate: {
    type: String,
    default: new Date(),
  },
});

module.exports = mongoose.model('user', UserSchema);
