const express = require('express');
const router = express.Router();

const {
  infoController,
  passEditController,
  deleteUserController,
} = require('../controllers/meController');

const authMiddleware = require('../middlewares/authMiddleware');

module.exports.infoRouter = router.get(
  '/users/me',
  authMiddleware,
  infoController
);

module.exports.passEditRouter = router.patch(
  '/users/me',
  authMiddleware,
  passEditController
);

module.exports.deleteUserRouter = router.delete(
  '/users/me',
  authMiddleware,
  deleteUserController
);
