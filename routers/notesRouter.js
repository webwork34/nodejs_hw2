const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');

const {
  addNoteController,
  showNotesController,
  showNoteByIdController,
  deleteNoteByIdController,
  updateNoteByIdController,
  checkToggleNoteByIdController,
} = require('./../controllers/notesController');

module.exports.addNoteRouter = router.post(
  '/notes',
  authMiddleware,
  addNoteController
);

module.exports.showNotesRouter = router.get(
  '/notes',
  authMiddleware,
  showNotesController
);

module.exports.showNoteByIdRouter = router.get(
  '/notes/:id',
  authMiddleware,
  showNoteByIdController
);

module.exports.deleteNoteByIdRouter = router.delete(
  '/notes/:id',
  authMiddleware,
  deleteNoteByIdController
);

module.exports.updateNoteByIdRouter = router.put(
  '/notes/:id',
  authMiddleware,
  updateNoteByIdController
);

module.exports.checkToggleNoteByIdRouter = router.patch(
  '/notes/:id',
  authMiddleware,
  checkToggleNoteByIdController
);
