const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
app.use(cors());

const PORT_APP = process.env.PORT || require('./config/serverConfig').PORT_APP;
const {PORT_DB, HOST, NAME_DB} = require('./config/dbConfig');

const authRouter = require('./routers/authRouter');

const {
  infoRouter,
  passEditRouter,
  deleteUserRouter,
} = require('./routers/meRouter');

const {
  addNoteRouter,
  showNotesRouter,
  showNoteByIdRouter,
  deleteNoteByIdRouter,
  updateNoteByIdRouter,
  checkToggleNoteByIdRouter,
} = require('./routers/notesRouter');

mongoose
  .connect(`mongodb://${HOST}:${PORT_DB}/${NAME_DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  })
  .then(() => console.log('MongoDB has been started...'))
  .catch(err => console.log(err));

app.use(express.json());
app.use('/api', authRouter);
app.use('/api', infoRouter);
app.use('/api', passEditRouter);
app.use('/api', deleteUserRouter);

app.use('/api', addNoteRouter);
app.use('/api', showNotesRouter);
app.use('/api', showNoteByIdRouter);
app.use('/api', deleteNoteByIdRouter);
app.use('/api', updateNoteByIdRouter);
app.use('/api', checkToggleNoteByIdRouter);

app.listen(PORT_APP, () =>
  console.log(`Server has been started on port ${PORT_APP}...`)
);
