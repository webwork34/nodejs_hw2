const jwt = require('jsonwebtoken');
const {SECRET} = require('./../config/authConfig');

const User = require('./../models/userModel');

module.exports.register = (req, res) => {
  const {username, password} = req.body;

  const user = new User({username, password});
  user
    .save()
    .then(() => res.status(201).json({status: 'success'}))
    .catch(err => res.status(500).json({status: err.message}));
};

module.exports.login = (req, res) => {
  const {username, password} = req.body;

  User.findOne({username, password})
    .exec()
    .then(user => {
      if (!user) {
        return res.status(400).json({status: "User hasn't found"});
      }
      // если все ок - должны подписать токен
      res.status(200).json({
        jwt_token: `Bearer ${jwt.sign(JSON.stringify(user), SECRET)}`,
      });
    })
    .catch(err => res.status(500).json({status: err.status}));
};
