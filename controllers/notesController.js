const Note = require('./../models/notesModel');

module.exports.addNoteController = (req, res) => {
  const {text} = req.body;

  const note = new Note({text, userId: req.user._id});

  note
    .save()
    .then(() => res.status(201).json({status: 'success'}))
    .catch(err => res.status(500).json({status: err.message}));
};

module.exports.showNotesController = (req, res) => {
  Note.find({userId: req.user._id})
    .exec()
    .then(data => {
      if (!data) {
        return res
          .status(400)
          .json({message: "This user hasn't made any notes yet"});
      }

      const newData = JSON.parse(JSON.stringify(data));
      newData.forEach(el => delete el.__v);

      res.status(200).json({notes: newData});
    })
    .catch(err => res.status(500).json({status: err.status}));
};

module.exports.showNoteByIdController = (req, res) => {
  Note.findOne({_id: req.params.id, userId: req.user._id})
    .exec()
    .then(data => {
      if (!data) {
        return res
          .status(400)
          .json({message: `Note with id ${req.params.id} hasn't found`});
      }

      const newData = JSON.parse(JSON.stringify(data));
      delete newData.__v;

      res.status(200).json({note: newData});
    })
    .catch(err => res.status(500).json({status: err.status}));
};

module.exports.deleteNoteByIdController = (req, res) => {
  Note.findOneAndDelete({_id: req.params.id, userId: req.user._id})
    .exec()
    .then(data => {
      if (!data) {
        return res
          .status(400)
          .json({message: `Note with id ${req.params.id} hasn't found`});
      }

      res.status(200).json({message: 'success'});
    })
    .catch(err => res.status(500).json({status: err.status}));
};

module.exports.updateNoteByIdController = (req, res) => {
  Note.findOneAndUpdate(
    {_id: req.params.id, userId: req.user._id},
    {$set: req.body}
  )
    .exec()
    .then(data => {
      if (!data) {
        return res
          .status(400)
          .json({message: `Note with id ${req.params.id} hasn't found`});
      }

      res.status(200).json({message: 'success'});
    })
    .catch(err => res.status(500).json({status: err.status}));
};

module.exports.checkToggleNoteByIdController = async (req, res) => {
  let noteStatus;

  await Note.findOne({_id: req.params.id, userId: req.user._id})
    .exec()
    .then(data => {
      if (!data) {
        return res
          .status(400)
          .json({message: `Note with id ${req.params.id} hasn't found`});
      }
      noteStatus = data.completed;
    })
    .catch(err => res.status(500).json({status: err.status}));

  Note.findOneAndUpdate(
    {_id: req.params.id, userId: req.user._id},
    {$set: {completed: noteStatus === false ? true : false}}
  )
    .exec()
    .then(data => {
      if (!data) {
        return res
          .status(400)
          .json({message: `Note with id ${req.params.id} hasn't found`});
      }

      res.status(200).json({message: 'success'});
    })
    .catch(err => res.status(500).json({status: err.status}));
};
