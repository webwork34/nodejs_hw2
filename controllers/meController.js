const mongoose = require('mongoose');
require('./../models/userModel');
const User = mongoose.model('user');

module.exports.infoController = (req, res) => {
  res.status(200).json({
    user: {
      _id: req.user._id,
      username: req.user.username,
      createdDate: req.user.createdDate,
    },
  });
};

module.exports.passEditController = async (req, res) => {
  const {oldPassword, newPassword} = req.body;

  if (!oldPassword || !newPassword) {
    return res
      .status(400)
      .json({message: 'You should pass old and new passwords'});
  }

  let oldPasswordDB;

  await User.findById(req.user._id)
    .exec()
    .then(user => {
      if (!user) {
        return res
          .status(500)
          .json({message: 'Something went wrong, try again later'});
      }

      oldPasswordDB = user.password;
    })
    .catch(err => res.status(500).json({status: err.status}));

  if (oldPassword !== oldPasswordDB) {
    return res
      .status(400)
      .json({message: 'Wrong old password. Check it and try again...'});
  }

  User.findByIdAndUpdate(req.user._id, {$set: {password: newPassword}})
    .exec()
    .then(() => res.status(200).json({message: 'success'}))
    .catch(err => res.status(500).json({status: err.status}));
};

module.exports.deleteUserController = (req, res) => {
  User.findByIdAndDelete(req.user._id)
    .then(user => {
      if (!user) return res.status(400).json({message: "User hasn't found"});
      res.json({message: 'Success'});
    })
    .catch(err => res.status(500).json({error: err.message}));
};
